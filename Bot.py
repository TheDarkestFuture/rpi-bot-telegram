# -*- coding: utf-8 -*-

import telebot # Librería de la API del bot.
from telebot import types # Tipos para la API del bot.
import time # Librería para hacer que el programa que controla el bot no se acabe.
import random
import datetime
import token
import os
import commands

TOKEN = 'TOKEN' #Nuestro token del bot

bot = telebot.TeleBot(TOKEN) # Creamos el objeto de nuestro bot.
#############################################
#Listener
def listener(messages): # Con esto, estamos definiendo una función llamada 'listener', que recibe como parámetro un dato llamado 'messages'.
    for m in messages: # Por cada dato 'm' en el dato 'messages'
        cid = m.chat.id # Almacenaremos el ID de la conversación.
        if m.content_type == 'text':
            print "[" + str(cid) + "]: " + m.text # Y haremos que imprima algo parecido a esto -> [52033876]: /start

bot.set_update_listener(listener) # Así, le decimos al bot que utilice como función escuchadora nuestra función 'listener' declarada arriba.
#############################################
#Funciones
@bot.message_handler(commands=['recuerda'])
def command_ayuda(m):
    cid = m.chat.id
    bot.send_message( cid, "Comandos Disponibles: /recuerda, /temperatura, /quien, /espacio, /informacion, /tiempo, /libre")


@bot.message_handler(commands=['temperatura'])
def command_temp(m):
        cid = m.chat.id
        #temp = os.system('sudo /opt/vc/bin/vcgencmd measure_temp')
        temp = commands.getoutput('sudo /opt/vc/bin/vcgencmd measure_temp')
        bot.send_message(cid, temp)


@bot.message_handler(commands=['saludo'])
def command_saludo(m):
        cid = m.chat.id
        saludo = commands.getoutput('./saludo.sh')
        bot.send_message(cid, saludo)


@bot.message_handler(commands=['espacio'])
def command_espacio(m):
        cid = m.chat.id
        info = commands.getoutput('df -h')
        bot.send_message(cid, info)

@bot.message_handler(commands=['quien'])
def command_who(m):
        cid = m.chat.id
        who = commands.getoutput('who')
        bot.send_message(cid, who)


@bot.message_handler(commands=['informacion'])
def command_screenfetch(m):
        cid = m.chat.id
        screenfetch = commands.getoutput('screenfetch -n')
        bot.send_message(cid, screenfetch)

@bot.message_handler(commands=['tiempo'])
def command_uptime(m):
        cid = m.chat.id
        uptime = commands.getoutput('uptime')
        bot.send_message(cid, uptime)

@bot.message_handler(commands=['libre'])
def command_free(m):
        cid = m.chat.id
        libre = commands.getoutput('free -m')
        bot.send_message(cid, libre)



#############################################
#Peticiones
bot.polling(none_stop=True) # Con esto, le decimos al bot que siga funcionando incluso si encuentra algun fallo.
